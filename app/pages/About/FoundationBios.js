import Luke from "svg/avatar-luke.svg";
import Eddie from "svg/avatar-eddie.svg";

export default [
  {
    name: "Luke Champine",
    svg: Luke,
    role: "President & Core Developer",
    social: [
      {
        title: "Github",
        url: "https://github.com/lukechampine",
      },
      {
        title: "Gitlab",
        url: "https://gitlab.com/lukechampine",
      },
      {
        title: "Twitter",
        url: "https://twitter.com/lukechampine",
      },
    ],
    content:
      "Sia captures that rebellious hacker spirit that spurns the current paradigm and, instead, builds something novel, powerful, esoteric. Code is eating the world; blockchains are just a continuation of that trend, extending it into the world of finance.",
  },
  {
    name: "Eddie Wang",
    svg: Eddie,
    role: "Chairman",
    social: [
      {
        title: "Github",
        url: "https://github.com/eddiewang",
      },
      {
        title: "Gitlab",
        url: "https://gitlab.com/eddiewang",
      },
      {
        title: "LinkedIn",
        url: "https://linkedin.com/in/eddieywang",
      },
      {
        title: "Twitter",
        url: "https://twitter.com/eddiepluswang",
      },
    ],
    content: `Distributed storage will be a key component to a fault-tolerant blockchain ecosystem. Sia is the only fully-decentralized product in this space with a thoughtful design and clear product path.`,
  },
];
